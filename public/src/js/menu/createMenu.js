window.onload = function (event) {
    const menu = [{key: 1, value: "Inicio", link: "home"},
        {key: 1, value: "Sobre", link: "about"},
        {key: 1, value: "Serviço", link: "service"},
        {key: 1, value: "Contato", link: "contact"},
    ];
    const menuController = document.getElementById('loadMenu');
    menu.forEach(addMenu => {
        menuController.insertAdjacentHTML('beforeend', `
            <li><a href="#${addMenu.link}">${addMenu.value}</a><hr></li>
        `);
    });
}