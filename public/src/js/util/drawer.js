function openMenu() {
    const openMenu = document.getElementById('openMenu');
    const bodyController = document.getElementById('bodyController');
    openMenu.classList.toggle('hidden');
    openMenu.classList.toggle('animate');
    bodyController.classList.toggle('blockScroll');
}

function closeMenu(){
    const closed = document.getElementById('openMenu');
    const bodyController = document.getElementById('bodyController');
    if (!closed.classList.contains('hidden')){
        bodyController.classList.remove('blockScroll');
        closed.classList.add('hidden');
    }
}
document.getElementById('openMenu').addEventListener('click', ()=>{
    closeMenu();
})

document.getElementById('menuDrawer').addEventListener('click', () => {
    openMenu();
});


